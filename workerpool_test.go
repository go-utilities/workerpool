package workerpool_test

import (
	"fmt"

	wp "gitlab.com/go-utilities/workerpool"
)

func Example() {
	// fibN is the result structure for Fibonacci calculation: Mapping of n to the
	// n-th Fibonacci number
	type fibN struct {
		n   int
		fib int
	}

	// fibonacci calculates the n-th Fibonacci number
	var fibonacci func(int) fibN
	fibonacci = func(n int) fibN {
		if n < 2 {
			return fibN{n: n, fib: n}
		}
		return fibN{n: n, fib: fibonacci(n-1).fib + fibonacci(n-2).fib}
	}

	// create worker pool with 10 workers
	pl := wp.NewPool(10)

	// submit tasks to worker pool. To not have to wait, that's done in a Go
	// routine
	go func() {
		for n := 0; n < 100; n++ {
			pl.In <- wp.Task{
				Name: "fibonacci",
				F: func(i interface{}) interface{} {
					return fibonacci(i.(int))
				},
				In: n}
		}
		close(pl.In)
	}()

	// retrieve results from worker pool
	for res := range pl.Out {
		fmt.Printf("%3d-th Fibonacci Number = %d\n", res.Out.(fibN).n, res.Out.(fibN).fib)
	}
}
