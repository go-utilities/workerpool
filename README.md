[![Go Reference](https://pkg.go.dev/badge/gitlab.com/go-utilities/file.svg)](https://pkg.go.dev/gitlab.com/go-utilities/workerpool)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/go-utilities/file)](https://goreportcard.com/report/gitlab.com/go-utilities/workerpool)
[![REUSE status](https://api.reuse.software/badge/gitlab.com/go-utilities/file)](https://api.reuse.software/info/gitlab.com/go-utilities/workerpool)

# Go Utilities: workerpool

Go package with utilities that support to execute tasks in a parallel manner with a pool of workers.
